from app.repositories.es.accommodation import AccommodationElasticRepo
from app.repositories.es.keyword import KeywordElasticRepo

if __name__ == '__main__':
    es_repos = [AccommodationElasticRepo(), KeywordElasticRepo()]
    for repo in es_repos:
        repo.remove_index_if_exist()
        repo.create_index_if_not_exist()
