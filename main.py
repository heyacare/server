# coding=utf-8
import logging
from flask_socketio import join_room, leave_room
import time
from flask import request

from apscheduler.schedulers.background import BackgroundScheduler

from app import app, socketio
from app.services import message as message_svc, favorite as favorite_svc

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def print_date_time():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))


# # register for jobs
# scheduler = BackgroundScheduler()
# scheduler.add_job(func=print_date_time, trigger="interval", seconds=60)
# scheduler.start()


@socketio.on("join")
def join_room_chat(data):
    if data['receiver_id'] == 0:
        return
    room = str(data['sender_id']) + 'to' + str(data['receiver_id'])
    print(room)
    join_room(room, request.sid)


@socketio.on("disconnect")
def disconnect():
    print("disconnect")


@socketio.on("message")
def handle_message(data):
    message = data['message']
    print(message)
    message_svc.save_message(message['content'], message['sender_id'], message['receiver_id'],
                             type_of_message=message['type_of_message'])
    room1 = str(message['receiver_id']) + 'to' + str(message['sender_id'])
    room2 = str(message['sender_id']) + 'to' + str(message['receiver_id'])
    socketio.emit("message", data, room=room1)
    socketio.emit("message", data, room=room2)
    return None


@socketio.on("join-favorite")
def join_favorite(data):
    if data['user_id'] == 0:
        return
    room = 'user' + str(data['user_id'])
    join_room(room, request.sid)


@socketio.on("favorite")
def handle_favorite(data):
    favorite_svc.create_favorite(user_id=data['user_id'], accommodation_id=data['accommodation_id'])


@socketio.on("join-notification")
def join_notification(data):
    if data['user_id'] == 0:
        return
    room = 'user' + str(data['user_id'])
    join_room(room, request.sid)


if __name__ == '__main__':
    socketio.run(app, host='localhost')
