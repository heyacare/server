import logging

import pytest

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


@pytest.fixture
def app_class(request, app):
    if request.cls is not None:
        request.cls.app = app
