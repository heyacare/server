import unittest

from app.helpers import string_utils
from app.tests import TestCase

normalize_phone_number_tests = [
    TestCase('0384721376', '0384721376'),
    TestCase('84384721376', '0384721376'),
    TestCase('+84384721376', '0384721376')
]

remove_vi_accent_tests = [
    TestCase('ăn', 'an'),
    TestCase('anh yêu em', 'anh yeu em'),
    TestCase('Phòng trọ gần các trường đại học lớn : Đại học Thương Mại, Đại học Sư phạm, Đại học Quốc gia, ...',
             'Phong tro gan cac truong đai hoc lon : Đai hoc Thuong Mai, Đai hoc Su pham, Đai hoc Quoc gia, ...')
]

normalize_text_tests = [
    TestCase('An', 'an'),
    TestCase('Chung ta khong thuoc ve nhau - Son Tung', 'chung ta khong thuoc ve nhau - son tung'),
    TestCase('Hello Nhat', 'hello nhat')
]


class NormalizePhoneNumber(unittest.TestCase):
    def test(self):
        for test in normalize_phone_number_tests:
            self.assertEqual(test.output, string_utils.normalize_phone_number(test.input))


class RemoveViAccent(unittest.TestCase):
    def test(self):
        for test in remove_vi_accent_tests:
            self.assertEqual(test.output, string_utils.remove_vi_accent(test.input))


class NormalizeText(unittest.TestCase):
    def test(self):
        for test in normalize_text_tests:
            self.assertEqual(test.output, string_utils.normalize_text(test.input))
