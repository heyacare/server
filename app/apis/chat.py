import logging

from flask_restplus import Resource
from flask import request

import app.apis.schema.request.user
import app.apis.schema.response.user
from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('chat', description='Chat operations')

_get_messages_req = ns.model('get_messages_req', app.apis.request.chat.get_messages_req)
_get_list_user_message_req = ns.model('get_list_user_message_req', app.apis.request.chat.get_list_user_message_req)


@ns.route('/messages', methods=['POST'])
class GetPendingOwner(Resource):
    @ns.expect(_get_messages_req, validate=True)
    def post(self):
        data = request.args or request.json
        print(data['receiver_id'], data['_page'], data['_limit'])
        return services.message.get_messages_for_user(data['receiver_id'], data['_page'], data['_limit'])


@ns.route('/get-users', methods=['POST'])
class ConfirmOwners(Resource):
    @ns.expect(_get_list_user_message_req, validate=True)
    def post(self):
        data = request.args or request.json
