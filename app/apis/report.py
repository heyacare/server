import logging

from flask_restplus import Resource
from flask import request

from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('report', description='Admin operations')


@ns.route('/get', methods=['POST'])
class GetFavorites(Resource):
    def post(self):
        data = request.args or request.json
        return services.report.get_reports(data['_page'], data['_limit'])


@ns.route('/create', methods=['POST'])
class CreateReport(Resource):
    def post(self):
        data = request.args or request.json
        return services.report.create_report(data['accommodation_id'], data['message'])
