# coding=utf-8
import logging
from flask_restplus import fields

from app.apis import apis

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

_city_data = apis.model('city_data', {
    'code': fields.String(),
    'name': fields.String()
})

_district_data = apis.model('district_data', {
    'code': fields.String(),
    'name': fields.String()
})

_ward_data = apis.model('district_data', {
    'code': fields.String(),
    'name': fields.String()
})

# full_city_res = apis.model('full_city_res', fields.List(fields.Nested(_city_data)))
city_res = apis.model('city_res', _city_data)

# full_district_res = apis.model('full_district_res', fields.List(fields.Nested(_district_data)))
district_res = apis.model('district_res', _district_data)

# full_ward_res = apis.model('full_ward_res', fields.List(fields.Nested(_ward_data)))
ward_res = apis.model('ward_res', _ward_data)
