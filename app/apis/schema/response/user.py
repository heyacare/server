# coding=utf-8

import logging
from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

user_res = {
    'id': fields.Integer(description="user's id"),
    'email': fields.String(description="user's email address"),
    'fullname': fields.String(description='fullname of user'),
    'avatar_url': fields.String(description='avatar url of user'),
    'address': fields.String(description='address of user'),
    'gender': fields.String(description='gender of user'),
    'role': fields.String(description='role of user (admin, seller, customer)'),
}

register_res = {
    'email': fields.String(description="email"),
    'fullname': fields.String(description="fullname"),
}
