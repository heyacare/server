# coding=utf-8
import logging

from app.apis import apis
from flask_restplus import fields

_logger = logging.getLogger(__name__)

rating_data = apis.model('rating_data', {
    'id': fields.Integer(),
    'user_name': fields.String(),
    'star': fields.Integer(),
    'comment': fields.String(),
    'created_at': fields.DateTime()
})

rating_response = apis.model('rating_response', {
    'ratings': fields.List(fields.Nested(rating_data)),
    'total': fields.Integer()
})
