import logging
from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

register_user_req = {
    'email': fields.String(required=True, description='user email address'),
    'phone_number': fields.String(required=False, fields='phone_number', description='phone number of user'),
    'fullname': fields.String(required=True, description='fullname of user'),
    'address': fields.String(required=False, description='address of user'),
    'password': fields.String(required=True, description='raw password of user'),
    'gender': fields.Integer(required=False, description='gender of user. 1 -> male')
}

login_req = {
    'username': fields.String(required=True, description='email or phone number'),
    'password': fields.String(required=True, description='password of user')
}

login_google_req = {
    'token': fields.String(required=True, description='google login token')
}

change_password_req = {
    'current_password': fields.String(required=True, description='current password'),
    'new_password': fields.String(required=True, description='new password')
}

change_profile_req = {
    'avatar_url': fields.String(required=False, description='new avatar url of user'),
    'address': fields.String(required=False, description='new address url of user'),
    'gender': fields.Integer(required=False, description='new gender of user. 1 -> male')
}