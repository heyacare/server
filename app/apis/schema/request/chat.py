import logging
from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

get_messages_req = {
    'receiver_id': fields.Integer(required=True, description="receiver id"),
    '_page': fields.Integer(required=True, description='paging, default to 1'),
    '_limit': fields.Integer(required=True, description='number of product in response, default to 10')
}

get_list_user_message_req = {
    '_page': fields.Integer(required=True, description='paging, default to 1'),
    '_limit': fields.Integer(required=True, description='number of product in response, default to 10')
}
