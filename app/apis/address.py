import app.apis
import logging
from app.extensions import Namespace
from app.services import address as service
from flask_restplus import Resource

__author__ = 'nhatns'

_logger = logging.getLogger(__name__)

ns = Namespace('address', description='Address operations')


@ns.route('/city', methods=['GET'])
class GetFullCity(Resource):
    ns.marshal_with(app.apis.response.address.city_res, as_list=True)

    def get(self):
        return service.get_full_city()


@ns.route('/district/<city_code>', methods=['GET'])
class GetDistrictByCityCode(Resource):
    ns.marshal_with(app.apis.response.address.district_res, as_list=True)

    def get(self, city_code: str):
        return service.get_full_district_by_city_code(city_code)


@ns.route('/ward/<district_code>', methods=['GET'])
class GetWardByDistrictCode(Resource):
    ns.marshal_with(app.apis.response.address.ward_res, as_list=True)

    def get(self, district_code: str):
        return service.get_full_ward_by_district_code(district_code)


@ns.route('/district/top10-for-rent', methods=['GET'])
class GetTop10DistrictForRent(Resource):
    def get(self):
        return service.get_top10_district_for_rent()
