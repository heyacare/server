import logging
from typing import List

from app import models as m
from app.models.mysql.notification import Notification


def save(notification: Notification):
    m.db.session.add(notification)
    m.db.session.commit()
    return notification


def save_notification_to_database(**kwargs):
    message = Notification(**kwargs)
    return save(message)


def get_notifications_for_user(user_id: int, page: int, limit: int) -> List[Notification]:
    return Notification.query.filter(Notification.owner_id == user_id) \
        .order_by(Notification.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit)


def get_number_notification_unread(user_id: int) -> int:
    return Notification.query.filter(Notification.owner_id == user_id,
                                     Notification.isRead == False).count()
