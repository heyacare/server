import logging
from typing import List

from app.models.mysql.city import City

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_all() -> List[City]:
    return City.query.all()


def get_by_code(code: str) -> City:
    return City.query.filter(City.code == code).first()
