import logging
from typing import List

from app.models.mysql.accommodation_type import AccommodationType

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_full_accommodation_type() -> List[AccommodationType]:
    return AccommodationType.query.all()


def get_accommodation_type_by_id(id: int) -> AccommodationType:
    return AccommodationType.query.filter(AccommodationType.id == id).first()
