import logging
from typing import List

from app import models as m
from app.models.mysql.favorite import Favorite


def save(favorite: Favorite):
    m.db.session.add(favorite)
    m.db.session.commit()
    return favorite


def save_favorite_to_database(**kwargs):
    message = Favorite(**kwargs)
    return save(message)


def get_favorites_for_user(user_id: int, page: int, limit: int) -> List[Favorite]:
    return Favorite.query.filter(Favorite.user_id == user_id) \
        .order_by(Favorite.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit)


def get_number_favorites_of_user(user_id: int) -> int:
    return Favorite.query.filter(Favorite.user_id == user_id).count()


def get_favorite_by_id(favorite_id):
    return Favorite.query.filter(Favorite.id == favorite_id).first()


def get_number_of_favorite_by_accommodation_id(accommodation_id: str):
    return Favorite.query.filter(Favorite.accommodation_id == accommodation_id).count()
