# coding=utf-8
import logging
from typing import List

from sqlalchemy.sql import func

from app import models as m

__author__ = 'nhatns'

from app.models.mysql.rating import Rating, RatingStatus

_logger = logging.getLogger(__name__)


def save(rating: Rating) -> Rating:
    m.db.session.add(rating)
    m.db.session.commit()
    return rating


def create_new_rating(**kwargs) -> Rating:
    rating = Rating(**kwargs)
    return save(rating)


def find_by_id(id: int) -> Rating:
    rating = Rating.query.filter(
        Rating.id == id, Rating.status == RatingStatus.Approved
    ).first()
    return rating or None


def find_all_rating(accommodation_id: str, page: int, limit: int) -> List[Rating]:
    ratings = Rating.query \
        .filter(Rating.accommodation_id == accommodation_id, Rating.status == RatingStatus.Approved) \
        .order_by(Rating.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit) \
        .all()
    return ratings


def find_all_rating_by_star(accommodation_id: str, star: int, page: int, limit: int) -> List[Rating]:
    ratings = Rating.query. \
        filter(Rating.star == star, Rating.accommodation_id == accommodation_id, Rating.status == RatingStatus.Approved) \
        .order_by(Rating.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit) \
        .all()
    return ratings


def get_average_rating_by_accommodation_id(accommodation_id: str):
    res = m.db.session.query(func.avg(Rating.star).label('average')).filter(
        Rating.accommodation_id == accommodation_id, Rating.status == RatingStatus.Approved).first()
    return extract_result(res)


def extract_result(result):
    try:
        return float(result._asdict().get('average'))
    except Exception:
        return 0


def find_pending_rating(page: int, limit: int) -> List[Rating]:
    ratings = Rating.query \
        .filter(Rating.status == RatingStatus.Pending) \
        .order_by(Rating.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit) \
        .all()
    return ratings


def find_pending_rating_by_ids(ids: List[int]) -> List[Rating]:
    ratings = Rating.query.filter(Rating.status == RatingStatus.Pending, Rating.id.in_(ids)).all()
    return ratings


def get_total_pending_rating():
    return Rating.query.filter(Rating.status == RatingStatus.Pending).count()
