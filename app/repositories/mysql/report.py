import logging
from typing import List

from app import models as m
from app.models.mysql.report import Report


def save(report: Report):
    m.db.session.add(report)
    m.db.session.commit()
    return report


def save_notification_to_database(**kwargs):
    message = Report(**kwargs)
    return save(message)


def get_reports(page: int, limit: int) -> List[Report]:
    return Report.query.filter(True) \
        .order_by(Report.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit)


def get_number_report_by_accommodation_id(accommodation_id) -> int:
    return Report.query.filter(Report.accommodation_id == accommodation_id).count()
