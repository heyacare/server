import logging

from app.models.mysql.accommodation_view import AccommodationView
from app import models as m

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def save(view: AccommodationView):
    m.db.session.add(view)
    m.db.session.commit()


def save_view_to_database(accommodation_id: str):
    view = AccommodationView(accommodation_id=accommodation_id)
    save(view)
