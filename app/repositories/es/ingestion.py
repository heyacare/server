# coding=utf-8
import logging
from typing import List

from config import ACCOMMODATION_INDEX

__author__ = 'nhatns'

_logger = logging.getLogger(__name__)


def upsert_accommodation(accommodation: dict) -> dict:
    from app.repositories.es import bulk_update
    bulk_update(ACCOMMODATION_INDEX, [accommodation], 'id')
    return accommodation


def upsert_accommodations(accommodations: List[dict]):
    from app.repositories.es import bulk_update
    bulk_update(ACCOMMODATION_INDEX, accommodations, 'id')
    return accommodations
