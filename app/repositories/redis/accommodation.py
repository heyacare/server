import logging

from app.repositories.redis import RedisCacheBase
from app.repositories.mysql import accommodation_type as accommodation_type_repo, rating as rating_repo

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_accommodation_type_by_type_id(accommodation_type_id: int):
    accommodation_type_redis = AccommodationTypeNameCache()
    return accommodation_type_redis.get(str(accommodation_type_id))


def get_full_accommodation_type():
    accommodation_type_redis = AccommodationTypeCache()
    return accommodation_type_redis.get('all')


class AccommodationTypeNameCache(RedisCacheBase):
    def __init__(self):
        super().__init__("accommodation_type::name")

    def fetch_data(self, key: str):
        accommodation_type = accommodation_type_repo.get_accommodation_type_by_id(int(key))
        return accommodation_type.name


class AccommodationTypeCache(RedisCacheBase):
    def __init__(self):
        super().__init__("accommodation::info")

    def fetch_data(self, key: str):
        accommodation_types = accommodation_type_repo.get_full_accommodation_type()
        return [accommodation_type.to_dict() for accommodation_type in accommodation_types]


class RatingCache(RedisCacheBase):

    def __init__(self):
        super().__init__("rating", ttl=300)

    def fetch_data(self, key: str):  # key is id of accommodation
        return rating_repo.get_average_rating_by_accommodation_id(key)
