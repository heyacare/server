# coding=utf-8
import logging

import click
from flask.cli import AppGroup

from app.repositories.mysql import user as user_repo
from app.services import user as user_service

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
admin_cli = AppGroup('admin', help="Backdoor by admin",
                     context_settings=CONTEXT_SETTINGS)


@admin_cli.command('set_password', help='Set user password')
@click.argument('email', metavar='{email}')
@click.argument('password', metavar='{password}')
def set_password(email, password):
    print("Setting password of user _{}_ to {}".format(email, password))
    user = user_repo.find_one_by_email(email)
    if not user:
        print("User not exist")
        return
    user_service.create_or_update_password(user, password)
    print("Done")
