from app.helpers.string_utils import normalize_text, remove_vi_accent

SPLITER = " | "


class Utilities:
    @staticmethod
    def pagination(page, page_size, total_items):
        """
        :param int page:
        :param int page_size:
        :param int total_items:
        :return: total pages, next page, previous page
        :rtype: int, int, int
        """
        total_pages = total_items // page_size if total_items % page_size == 0 \
            else (total_items // page_size) + 1
        next_page = page + 1 if page < total_pages else None
        previous_page = page - 1 if page > 1 else None
        return total_pages, next_page, previous_page

    @staticmethod
    def reformat_accommodation_search_params(args):
        args['q_source'] = args.get('q')

        args = Utilities.reformat_search_text_search_params(args)
        return args

    @staticmethod
    def reformat_search_text_search_params(args):
        args['_limit'] = args.get('_limit') or 10
        if args.get('_limit') > 1000 or args.get('_limit') < 1:
            args['_limit'] = 10
        args['_page'] = args.get('_page') or 1
        args['_page'] = max(args['_page'], 1)

        search_text = args.get('q')
        search_text = normalize_text(search_text)
        args['search_text'] = search_text
        args['q'] = remove_vi_accent(search_text) if search_text is not None else None

        return args


class Converter:
    @staticmethod
    def reformat_accommodation(accommodation):
        search_text = Converter.to_keyword_search_new(accommodation)
        accommodation['search_text'] = search_text.lower()
        accommodation['search_text_no_tone'] = remove_vi_accent(search_text)

    @staticmethod
    def to_keyword_search_new(obj):
        title = obj.get('title', '')
        description = obj.get('description', '')
        id = obj.get('id', '')
        nearby_locations = obj.get('nearby_locations', None)
        full_nearby_locations_string = Converter.get_nearby_locations_string(nearby_locations)
        full_address = obj.get('full_address', '')

        return SPLITER.join([
            title, description,
            *[id for _ in range(2)],
            *[full_nearby_locations_string for _ in range(4)],
            *[full_address for _ in range(4)]
        ])

    @staticmethod
    def get_nearby_locations_string(nearby_locations):
        if not nearby_locations:
            return ''
        return SPLITER.join([location['name'] for location in nearby_locations])
