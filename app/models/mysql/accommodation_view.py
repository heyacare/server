import logging

from app.models import db
from app.models.mysql.base import TimestampMixin
from datetime import datetime

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class AccommodationView(db.Model, TimestampMixin):
    __tablename__ = 'accommodation_view'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)
    date_of_access = db.Column(db.Date, default=datetime.now().date())

    accommodation = db.relationship('Accommodation', back_populates='views')
