import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class OwnerStatus():
    Pending = 0
    Approved = 1
    Banned = -1


class Owner(db.Model, TimestampMixin):
    """
    Contains information of owner table.
    """
    __tablename__ = 'owner'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    status = db.Column(db.Integer, default=OwnerStatus.Pending)
    identity_card_number = db.Column(db.String(255), nullable=False)

    user = db.relationship('User', back_populates='owner')

    def to_dict_register(self):
        return {
            'id': self.user_id
        }

    def to_dict(self):
        return {
            'id': self.user_id,
            'identity_card_number': self.identity_card_number,
            'fullname': self.user.fullname,
            "email": self.user.email,
            "phone_number": self.user.phone_number,
            "gender": "Nam" if self.user.gender else "Nữ",
            "avatar_url": self.user.avatar_url,
            "address": self.user.address
        }

    def _get_user_status(self):
        if self.status == OwnerStatus.Banned:
            return 'Chặn'
        if self.status == OwnerStatus.Approved:
            return 'Hoạt động'
        return 'Đang phê duyệt'
