import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class NearByLocation(db.Model, TimestampMixin):
    __tablename__ = 'nearby_location'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)
    name = db.Column(db.String(255), nullable=False)

    accommodation = db.relationship('Accommodation', back_populates='nearby_locations')

    def get_name(self):
        return self.name
