import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class Report(db.Model, TimestampMixin):
    __tablename__ = 'report'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(3000), nullable=False)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)

    # sender = db.relationship('User', back_populates='sender_reports')
    # receiver = db.relationship('User', back_populates='receiver_reports')
    accommodation = db.relationship('Accommodation', back_populates='reports')

    def to_dict(self):
        return {
            'id': self.id,
            'accommodation': self.accommodation.to_short_info_dict()
        }
