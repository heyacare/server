import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class AccommodationType(db.Model, TimestampMixin):
    __tablename__ = 'accommodation_type'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)

    accommodations = db.relationship('Accommodation', back_populates='accommodation_type')

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }
