import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class AccommodationImage(db.Model, TimestampMixin):
    __tablename__ = 'accommodation_image'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)
    image_url = db.Column(db.String(255), nullable=False)

    accommodation = db.relationship('Accommodation', back_populates='images')

    def get_url(self):
        return self.image_url
