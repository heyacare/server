import logging

from app.models import db
from app.models.mysql.base import TimestampMixin
from app.repositories.redis import user as user_cache

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class RatingStatus:
    Pending = 0
    Approved = 1
    Block = -1


class Rating(db.Model, TimestampMixin):
    __tablename__ = 'rating'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    accommodation_id = db.Column(db.String(30), db.ForeignKey('accommodation.id'), default=False)
    star = db.Column(db.Integer)
    comment = db.Column(db.Text(1000))
    status = db.Column(db.Integer, default=RatingStatus.Pending)

    # relationship:
    user = db.relationship('User', back_populates='ratings')
    accommodation = db.relationship('Accommodation', back_populates='ratings')

    def to_dict(self):
        return {
            'id': self.id,
            'user_name': user_cache.get_user_name_from_cache(self.user_id),
            'star': self.star,
            'comment': self.comment,
            'created_at': self.created_at,
            'status': self.get_status()
        }

    def get_status(self):
        if self.status == RatingStatus.Pending:
            return 'Pending'
        if self.status == RatingStatus.Approved:
            return 'Approved'
        return 'Block'
