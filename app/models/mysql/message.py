import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class MessageType:
    Text = 0
    Image = 1


class Message(db.Model, TimestampMixin):
    __tablename__ = 'message'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(3000), nullable=False)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    receiver_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    type_of_message = db.Column(db.Integer, nullable=False, default=MessageType.Text)

    def to_dict(self):
        return {
            'content': self.content,
            'sender_id': self.sender_id,
            'receiver_id': self.receiver_id,
            'type_of_message': self.type_of_message
        }
