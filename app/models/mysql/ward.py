import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class Ward(db.Model, TimestampMixin):
    __tablename__ = 'ward'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    name = db.Column(db.String(255), nullable=False)
    code = db.Column(db.String(13), nullable=False, primary_key=True)
    district_code = db.Column(db.String(13), db.ForeignKey('district.code'), nullable=False)

    # relation
    district = db.relationship('District', back_populates='wards')
    accommodations = db.relationship('Accommodation', back_populates='ward')

    def to_dict(self):
        return {
            'code': self.code,
            'name': self.name
        }
