import logging
import json

from app.models import db
from app.models.mysql.base import TimestampMixin
from app.repositories.redis import user, address, accommodation
from app.helpers.time import date_time_to_iso_string

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class AccommodationStatus:
    Pending = 0
    Approved = 1
    Block = -1


class BathroomType:
    Closed = 0
    NotClosed = 1


class KitchenType:
    Private = 0
    Public = 1
    Nothing = -1


class Accommodation(db.Model, TimestampMixin):
    __tablename__ = 'accommodation'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.String(225), primary_key=True)
    # owner:
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    # title:
    title = db.Column(db.String(255), nullable=False)
    # address:
    """
    eg:
    street_address: Số 79 ngõ 59 Khúc Thừa Dụ
    """
    street_address = db.Column(db.String(255), nullable=False)
    ward_code = db.Column(db.String(13), db.ForeignKey('ward.code'), nullable=False)

    description = db.Column(db.String(3000), nullable=True)
    # type of accommodation:
    accommodation_type_id = db.Column(db.Integer, db.ForeignKey('accommodation_type.id'), nullable=False)
    # number of room:
    number_of_room = db.Column(db.Integer, nullable=False)
    # amount payable per month:
    price = db.Column(db.Integer, nullable=False)
    # area:
    area = db.Column(db.Integer, nullable=False)
    # is stay with the owner?
    is_stay_with_the_owner = db.Column(db.Boolean, nullable=False)
    # bathroom:
    bathroom_type = db.Column(db.Integer, nullable=False, default=BathroomType.NotClosed)
    has_electric_water_heater = db.Column(db.Boolean, nullable=False, default=False)
    # kitchen:
    kitchen_type = db.Column(db.Integer, nullable=False, default=KitchenType.Nothing)
    # air conditioning:
    has_air_conditioning = db.Column(db.Boolean, nullable=False, default=False)
    # balcony:
    has_balcony = db.Column(db.Boolean, nullable=False, default=False)
    # amount electricity money payable per month:
    electricity_price = db.Column(db.Integer, nullable=False)
    # amount water money payable per month:
    water_price = db.Column(db.Integer, nullable=False)
    # display time:
    number_of_days_display = db.Column(db.Integer, nullable=False, default=0)
    approval_time = db.Column(db.TIMESTAMP, nullable=True)
    display_time = db.Column(db.TIMESTAMP, nullable=True)

    # status of post:
    status = db.Column(db.Integer, default=AccommodationStatus.Pending)

    # accommodation is rented:
    is_rented = db.Column(db.Boolean, default=False)

    # number of views:
    number_of_views = db.Column(db.Integer, default=0)

    # geo location:
    location_lat = db.Column(db.Float)
    location_lon = db.Column(db.Float)

    # relationship:
    ward = db.relationship('Ward', back_populates='accommodations')
    accommodation_type = db.relationship('AccommodationType', back_populates='accommodations')
    owner = db.relationship('User', back_populates='accommodations')
    images = db.relationship('AccommodationImage', back_populates='accommodation')
    nearby_locations = db.relationship('NearByLocation', back_populates='accommodation')
    views = db.relationship('AccommodationView', back_populates='accommodation')
    notifications = db.relationship('Notification', back_populates='accommodation')
    favorites = db.relationship('Favorite', back_populates='accommodation')
    reports = db.relationship('Report', back_populates='accommodation')
    ratings = db.relationship('Rating', back_populates='accommodation')

    def get_bathroom_type(self):
        if self.bathroom_type == BathroomType.NotClosed:
            return 'Chung'
        return 'Riêng'

    def get_kitchen_type(self):
        if self.kitchen_type == KitchenType.Nothing:
            return "Không nấu ăn"
        if self.kitchen_type == KitchenType.Public:
            return "Nấu ăn chung"
        return "Nấu ăn riêng"

    def get_status(self):
        if self.status == AccommodationStatus.Pending:
            return "Đang chờ"
        if self.status == AccommodationStatus.Approved:
            return "Đã phê duyệt"
        return "Bị từ chối"

    def to_short_info_dict(self):
        return {
            'id': self.id,
            'title': self.title
        }

    def to_dict(self):
        return {
            'id': self.id,
            'owner': json.loads(user.get_user_from_cache(self.owner_id)),
            'title': self.title,
            'description': self.description,
            'street_address': self.street_address,
            'address': address.get_full_address_by_ward_code(self.ward_code),
            'accommodation_type': accommodation.get_accommodation_type_by_type_id(self.accommodation_type_id),
            'number_of_room': self.number_of_room,
            'price': self.price,
            'area': self.area,
            'electricity_price': self.electricity_price,
            'water_price': self.water_price,
            'is_stay_with_the_owner': self.is_stay_with_the_owner,
            'is_rented': self.is_rented,
            'bathroom_type': self.get_bathroom_type(),
            'kitchen_type': self.get_kitchen_type(),
            'has_electric_water_heater': self.has_electric_water_heater,
            'has_balcony': self.has_balcony,
            'has_air_conditioning': self.has_air_conditioning,
            'number_of_views': self.number_of_views,
            'created_at': self.approval_time,
            'images': [image.get_url() for image in self.images],
            'nearby_locations': [location.get_name() for location in self.nearby_locations],
            'geometry': {
                'location': {
                    'lat': self.location_lat,
                    'lon': self.location_lon
                }
            },
            'city': self.ward.district.city.to_dict(),
            'ward': self.ward.to_dict(),
            'district': self.ward.district.to_dict()
        }

    def to_top10_dict(self):
        return {
            'id': self.id,
            'owner': json.loads(user.get_user_from_cache(self.owner_id)),
            'title': self.title,
            'description': self.description,
            'full_address': self.street_address + ', ' + address.get_full_address_by_ward_code(self.ward_code),
            'accommodation_type': {
                'id': self.accommodation_type_id,
                'name': accommodation.get_accommodation_type_by_type_id(self.accommodation_type_id)
            },
            'nearby_locations': [{'name': location.get_name()} for location in self.nearby_locations],
            'images': [{'url': image.get_url()} for image in self.images],
            'price': self.price,
            'area': self.area,
            'created_at': date_time_to_iso_string(self.approval_time),
            'geometry': {
                'location': {
                    'lat': self.location_lat,
                    'lon': self.location_lon
                }
            }
        }

    def to_elastic_dict(self):
        return {
            'id': self.id,
            'owner': json.loads(user.get_user_from_cache(self.owner_id)),
            'title': self.title,
            'description': self.description,
            'full_address': self.street_address + ', ' + address.get_full_address_by_ward_code(self.ward_code),
            'accommodation_type': {
                'id': self.accommodation_type_id,
                'name': accommodation.get_accommodation_type_by_type_id(self.accommodation_type_id)
            },
            'price': self.price,
            'area': self.area,
            'electricity_price': self.electricity_price,
            'water_price': self.water_price,
            'is_stay_with_the_owner': self.is_stay_with_the_owner,
            'bathroom_type': self.bathroom_type,
            'has_electric_water_heater': self.has_electric_water_heater,
            'has_balcony': self.has_balcony,
            'has_air_conditioning': self.has_air_conditioning,
            'created_at': date_time_to_iso_string(self.approval_time),
            'display_time_ranges': {
                'start': date_time_to_iso_string(self.approval_time),
                'end': date_time_to_iso_string(self.display_time)
            },
            'city': self.ward.district.city.to_dict(),
            'ward': self.ward.to_dict(),
            'district': self.ward.district.to_dict(),
            'geometry': {
                'location': {
                    'lat': self.location_lat,
                    'lon': self.location_lon
                }
            },
            'nearby_locations': [{'name': location.get_name()} for location in self.nearby_locations],
            'images': [{'url': image.get_url()} for image in self.images],
        }

    def to_accommodation_owner_dict(self):
        rating_cache = accommodation.RatingCache()
        return {
            'id': self.id,
            'title': self.title,
            'full_address': self.street_address + ', ' + address.get_full_address_by_ward_code(self.ward_code),
            'created_at': self.created_at,
            'status': self.get_status(),
            'number_of_views': self.number_of_views,
            'average_rating': rating_cache.get(self.id),
            'is_rented': self.is_rented,
            'images': [image.get_url() for image in self.images],
            'display_time': self.display_time
        }

    def to_accommodation_admin_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'created_at': date_time_to_iso_string(self.created_at),
            'owner': {
                'id': self.owner.id,
                'fullname': self.owner.fullname
            },
            'images': [image.get_url() for image in self.images]
        }

    def to_edit_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'area': self.area,
            'electricity_price': self.electricity_price,
            'water_price': self.water_price,
            'city_code': self.ward.district.city_code,
            'district_code': self.ward.district_code,
            'ward_code': self.ward_code,
            'accommodation_type_id': self.accommodation_type_id,
            'bathroom_type': self.bathroom_type,
            'kitchen_type': self.kitchen_type,
            'number_of_room': self.number_of_room,
            'street_address': self.street_address,
            'number_of_days_display': self.number_of_days_display,
            'images': [image.get_url() for image in self.images],
            'nearby_locations': [location.get_name() for location in self.nearby_locations],
            'is_stay_with_the_owner': self.is_stay_with_the_owner,
            'has_electric_water_heater': self.has_electric_water_heater,
            'has_air_conditioning': self.has_air_conditioning,
            'has_balcony': self.has_balcony,
            'price': self.price
        }
