import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class District(db.Model, TimestampMixin):
    __tablename__ = 'district'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    name = db.Column(db.String(255), nullable=False)
    code = db.Column(db.String(13), nullable=False, primary_key=True)
    city_code = db.Column(db.String(13), db.ForeignKey('city.code'), nullable=False)

    # relation
    city = db.relationship('City', back_populates='districts')
    wards = db.relationship('Ward', back_populates='district')

    def to_dict(self):
        return {
            'code': self.code,
            'name': self.name
        }
