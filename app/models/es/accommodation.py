# coding=utf-8
import logging

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

basic_details = {
    "id": {
        "type": "keyword"
    },
    "title": {
        "type": "text",
    },
    "description": {
        "type": "text",
    },
    "accommodation_type": {
        "type": "nested",
        "properties": {
            "id": {
                "type": "keyword"
            },
            "name": {
                "type": "keyword"
            }
        }
    },
    "price": {
        "type": "integer",
    },
    "electricity_price": {
        "type": "integer",
    },
    "water_price": {
        "type": "integer",
    },
    "area": {
        "type": "integer",
    },
    "full_address": {
        "type": "text",
    },
    "bathroom_type": {
        "type": "integer",
    },
    "kitchen_type": {
        "type": "integer"
    },
    "has_electric_water_heater": {
        "type": "boolean"
    },
    "has_balcony": {
        "type": "boolean"
    },
    "has_air_conditioning": {
        "type": "boolean"
    },
    "status": {
        "type": "text"
    },
    "display_time_ranges": {
        "type": "nested",
        "properties": {
            "end": {
                "type": "date",
                "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
            },
            "start": {
                "type": "date",
                "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
            }
        }
    },
    "keyword_generated": {
        "type": "boolean",
    },
    "images": {
        "type": "nested",
        "dynamic": "strict",
        "properties": {
            "label": {
                "type": "keyword",
                "index": False
            },
            "path": {
                "type": "keyword",
                "index": False
            },
            "priority": {
                "type": "integer",
                "index": False
            },
            "url": {
                "type": "keyword",
                "index": False
            }
        }
    },
    "nearby_locations": {
        "type": "nested",
        "dynamic": "strict",
        "properties": {
            "label": {
                "type": "keyword",
                "index": False
            },
            "path": {
                "type": "keyword",
                "index": False
            },
            "priority": {
                "type": "integer",
                "index": False
            },
            "name": {
                "type": "keyword",
                "index": False
            }
        }
    },
    "owner": {
        "type": "nested",
        "properties": {
            "id": {
                "type": "integer"
            },
            "name": {
                "type": "keyword",
                "index": False
            },
            "phone_number": {
                "type": "text",
                "index": False
            }
        }
    },
    "geometry": {
        "properties": {
            "location": {
                "type": "geo_point"
            }
        }
    },
    "city": {
        "type": "nested",
        "dynamic": "strict",
        "properties": {
            "code": {
                "type": "keyword"
            },
            "name": {
                "type": "keyword",
                "index": False
            }
        }
    },
    "district": {
        "type": "nested",
        "dynamic": "strict",
        "properties": {
            "code": {
                "type": "keyword"
            },
            "name": {
                "type": "keyword",
                "index": False
            }
        }
    },
    "ward": {
        "type": "nested",
        "dynamic": "strict",
        "properties": {
            "code": {
                "type": "keyword"
            },
            "name": {
                "type": "keyword",
                "index": False
            }
        }
    },
    "search_text": {
        "type": "text",
        "fields": {
            "raw": {
                "type": "keyword"
            }
        }
    },
    "search_text_no_tone": {
        "type": "text",
        "fields": {
            "raw": {
                "type": "keyword"
            }
        }
    },
    "created_at": {
        "type": "date",
        "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
    },
    "updated_at": {
        "type": "date",
        "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
    }
}

rating_details = {
    "rating": {
        "properties": {
            "average_point": {
                "type": "scaled_float",
                "scaling_factor": 10
            },
            "vote_count": {
                "type": "keyword"
            }
        }
    }
}

accommodation_details = {
    **basic_details,
    **rating_details
}

mappings = {
    "properties": {
        **accommodation_details
    }
}

settings = {
    "index": {
        "max_result_window": 500000,
        "number_of_shards": "1",
        "analysis": {
            "filter": {
                "synonym_filter": {
                    "type": "synonym",
                    "synonyms": []
                }
            },
            "analyzer": {
                "synonym_analyzer": {
                    "filter": [
                        "lowercase",
                        "synonym_filter"
                    ],
                    "tokenizer": "standard"
                }
            }
        }
    }
}
