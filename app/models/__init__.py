import logging

import flask_bcrypt as _fb
import flask_migrate as _fm
import flask_sqlalchemy as _fs
from dotenv import load_dotenv

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

from config import _DOT_ENV_PATH

load_dotenv(_DOT_ENV_PATH)

db = _fs.SQLAlchemy()
migrate = _fm.Migrate(db=db)
bcrypt = _fb.Bcrypt()


def init_app(app, **kwargs):
    db.app = app
    db.init_app(app)
    migrate.init_app(app)
    _logger.info('Start app in {env} environment with database: {db}'.format(
        env=app.config['ENV_MODE'],
        db=app.config['SQLALCHEMY_DATABASE_URI']
    ))


# import model to app.
from .mysql.register import Register
from .mysql.user import User
from .mysql.password import Password
from .mysql.owner import Owner
from .mysql.image import Image
from .mysql.city import City
from .mysql.district import District
from .mysql.ward import Ward
from .mysql.accommodation import Accommodation
from .mysql.accommodation_image import AccommodationImage
from .mysql.rating import Rating
from .mysql.accommodation_type import AccommodationType
from .mysql.nearby_location import NearByLocation
from .mysql.accommodation_view import AccommodationView
from .mysql.notification import Notification
from .mysql.message import Message
from .mysql.favorite import Favorite
from .mysql.report import Report
