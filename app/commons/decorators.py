from functools import wraps

from flask_jwt_extended import jwt_required, get_jwt_identity

from app.extensions.custom_exception import InvalidTokenException, PermissionException
from app.models.mysql.user import UserRole
from app.services import user as user_service


@jwt_required
def get_email_from_jwt_token():
    email = get_jwt_identity()
    return email


# use for api required login and set email to kwargs
def login_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            kwargs['email'] = get_email_from_jwt_token()
        except:
            raise InvalidTokenException("Invalid token or Signal expired")
        return func(*args, **kwargs)

    return wrap_func


# use for api required admin
def admin_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role != UserRole.Admin:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("Admin required")
        return func(*args, **kwargs)

    return wrap_func


# use for api required owner
def owner_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role != UserRole.Owner:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("Owner required")
        return func(*args, **kwargs)

    return wrap_func


# use for api required owner or admin
def admin_or_owner_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role != UserRole.Admin and user.role != UserRole.Owner:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("Admin or Owner required")
        return func(*args, **kwargs)

    return wrap_func


# use for api required owner
def is_not_the_owner_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role == UserRole.Owner:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("Required is not the owner")
        return func(*args, **kwargs)

    return wrap_func


# use for api required renter
def renter_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role != UserRole.Renter:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("Renter required")
        return func(*args, **kwargs)

    return wrap_func


def user_required(func):
    @wraps(func)
    def wrap_func(*args, **kwargs):
        try:
            # Todo: check token from redis
            email = get_email_from_jwt_token()
            user = user_service.find_one_by_email(email)
            if not user:
                raise Exception()
            if user.role != UserRole.Renter and user.role != UserRole.Admin and user.role != UserRole.Owner:
                raise Exception()
            kwargs['user'] = user
        except:
            raise PermissionException("User required")
        return func(*args, **kwargs)

    return wrap_func
