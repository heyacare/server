# coding=utf-8
import logging

from app.repositories.es import ingestion as repo

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def upsert_accommodation(accommodation):
    accommodation = repo.upsert_accommodation(accommodation)
    del accommodation['search_text']
    del accommodation['search_text_no_tone']
    return accommodation
