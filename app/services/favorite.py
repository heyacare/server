from app.commons.decorators import user_required
from app.models import Favorite
from app.models.mysql.favorite import FavoriteStatus
from app.repositories.mysql import favorite as repo


@user_required
def create_favorite(accommodation_id: int, **kwargs):
    user = kwargs['user']
    favorite = Favorite(user_id=user.id, accommodation_id=accommodation_id)
    return repo.save(favorite)


@user_required
def get_favorite(page: int, limit: int, **kwargs):
    user = kwargs['user']
    favorites = repo.get_favorites_for_user(user.id, page, limit)
    return [favorite.to_dict() for favorite in favorites]


def remove_favorite(favorite_id):
    favorite = repo.get_favorite_by_id(favorite_id)
    favorite.is_active = FavoriteStatus.InActive
    return repo.save(favorite)
