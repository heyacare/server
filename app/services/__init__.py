import logging
import threading

from flask_mail import Mail

from . import user
from . import register
from . import password
from . import mail_service
from . import owner
from . import file
from . import admin
from . import keyword
from . import accommodation
from . import rating
from . import message
from . import notification
from . import favorite
from . import report

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

accommodation_lock = threading.Lock()
my_mail = Mail()


def init_app(app):
    my_mail.init_app(app)
