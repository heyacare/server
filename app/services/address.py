import json
import logging
from typing import List

from app.repositories.redis import address as address_cache

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_full_city() -> List[dict]:
    cities = address_cache.get_full_city()
    cities = json.loads(cities)
    return cities


def get_full_district_by_city_code(city_code: str) -> List[dict]:
    districts = address_cache.get_full_district_by_city_code(city_code)
    districts = json.loads(districts)
    return districts


def get_full_ward_by_district_code(district_code: str) -> List[dict]:
    wards = address_cache.get_full_ward_by_district_code(district_code)
    wards = json.loads(wards)
    return wards
