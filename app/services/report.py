from app.commons.decorators import user_required
from app.models import Report
from app.repositories.mysql import report as repo


@user_required
def create_report(accommodation_id: int, message: str, **kwargs):
    user = kwargs['user']
    report = Report(sender_id=user.id, accommodation_id=accommodation_id, message=message)
    return repo.save(report)


@user_required
def get_reports(page: int, limit: int, **kwargs):
    reports = repo.get_reports(page, limit)
    return [report.to_dict() for report in reports]
