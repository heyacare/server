from app.commons.decorators import admin_or_owner_required
from app.models import Message
from app.repositories.mysql import message as repo


def save_message(content: str, sender_id: int, receiver_id: int, type_of_message: str):
    message = Message(content=content, sender_id=sender_id, receiver_id=receiver_id, type_of_message=type_of_message)
    return repo.save(message)


@admin_or_owner_required
def get_messages_for_user(receiver_id: int, _page: int, _limit: int, **kwargs):
    print(receiver_id, _page, _limit)
    sender = kwargs.get('user')
    if receiver_id == 0:
        return []
    messages = repo.get_messages_for_user(sender_id=sender.id, receiver_id=receiver_id, page=_page, limit=_limit)
    messages = [message.to_dict() for message in messages]
    res = []
    for i in reversed(range(len(messages))):
        res.append(messages[i])
    return res
