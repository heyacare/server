"""ward

Revision ID: 900d773b49db
Revises: 15ffde3960f9
Create Date: 2020-12-19 16:42:51.416877

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '900d773b49db'
down_revision = '15ffde3960f9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    table = op.create_table('ward',
                            sa.Column('name', sa.String(length=255), nullable=False),
                            sa.Column('code', sa.String(length=13), nullable=False),
                            sa.Column('district_code', sa.String(length=13), nullable=False),
                            sa.Column('created_at', sa.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
                            sa.Column('updated_at', sa.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
                            sa.ForeignKeyConstraint(['district_code'], ['district.code'], ),
                            sa.PrimaryKeyConstraint('code')
                            )

    data = [
        {
            "code": "0001",
            "name": " Cống Vị",
            "district_code": "0001"
        },
        {
            "code": "0002",
            "name": "Điện Biên",
            "district_code": "0001"
        },
        {
            "code": "0003",
            "name": "Đội Cấn",
            "district_code": "0001"
        },
        {
            "code": "0004",
            "name": "Giảng Võ",
            "district_code": "0001"
        },
        {
            "code": "0005",
            "name": "Kim Mã",
            "district_code": "0001"
        },
        {
            "code": "0006",
            "name": "Liễu Giai",
            "district_code": "0001"
        },
        {
            "code": "0007",
            "name": "Ngọc Hà",
            "district_code": "0001"
        },
        {
            "code": "0008",
            "name": "Ngọc Khánh",
            "district_code": "0001"
        },
        {
            "code": "0009",
            "name": "Nguyễn Trung Trực",
            "district_code": "0001"
        },
        {
            "code": "0010",
            "name": "Phúc Xá",
            "district_code": "0001"
        },
        {
            "code": "0011",
            "name": "Quán Thánh",
            "district_code": "0001"
        },
        {
            "code": "0012",
            "name": "Thành Công",
            "district_code": "0001"
        },
        {
            "code": "0013",
            "name": "Trúc Bạch",
            "district_code": "0001"
        },
        {
            "code": "0014",
            "name": "Cổ Nhuế 1",
            "district_code": "0002"
        },
        {
            "code": "0015",
            "name": "Cổ Nhuế 2",
            "district_code": "0002"
        },
        {
            "code": "0016",
            "name": "Đông Ngạc",
            "district_code": "0002"
        },
        {
            "code": "0017",
            "name": "Đức Thắng",
            "district_code": "0002"
        },
        {
            "code": "0018",
            "name": "Liên Mạc",
            "district_code": "0002"
        },
        {
            "code": "0019",
            "name": "Minh Khai",
            "district_code": "0002"
        },
        {
            "code": "0020",
            "name": "Thụy Phương",
            "district_code": "0002"
        },
        {
            "code": "0021",
            "name": "Thượng Cát",
            "district_code": "0002"
        },
        {
            "code": "0022",
            "name": "Tây Tựu",
            "district_code": "0002"
        },
        {
            "code": "0023",
            "name": "Phú Diễn",
            "district_code": "0002"
        },
        {
            "code": "0024",
            "name": "Phúc Diễn",
            "district_code": "0002"
        },
        {
            "code": "0025",
            "name": "Xuân Đỉnh",
            "district_code": "0002"
        },
        {
            "code": "0026",
            "name": "Xuân Tảo",
            "district_code": "0002"
        },
        {
            "code": "0027",
            "name": "Dịch Vọng",
            "district_code": "0003"
        },
        {
            "code": "0028",
            "name": "Dịch Vọng Hậu",
            "district_code": "0003"
        },
        {
            "code": "0029",
            "name": "Mai Dịch",
            "district_code": "0003"
        },
        {
            "code": "0030",
            "name": "Nghĩa Đô",
            "district_code": "0003"
        },
        {
            "code": "0031",
            "name": "Nghĩa Tân",
            "district_code": "0003"
        },
        {
            "code": "0032",
            "name": "Quan Hoa",
            "district_code": "0003"
        },
        {
            "code": "0033",
            "name": "Trung Hòa",
            "district_code": "0003"
        },
        {
            "code": "0034",
            "name": "Yên Hòa",
            "district_code": "0003"
        },
        {
            "code": "0035",
            "name": "Cát Linh",
            "district_code": "0004"
        },
        {
            "code": "0036",
            "name": "Hàng Bột",
            "district_code": "0004"
        },
        {
            "code": "0037",
            "name": "Khâm Thiên",
            "district_code": "0004"
        },
        {
            "code": "0038",
            "name": "Khương Thượng",
            "district_code": "0004"
        },
        {
            "code": "0039",
            "name": "Kim Liên",
            "district_code": "0004"
        },
        {
            "code": "0040",
            "name": "Láng Hạ",
            "district_code": "0004"
        },
        {
            "code": "0041",
            "name": "Láng Thượng",
            "district_code": "0004"
        },
        {
            "code": "0042",
            "name": "Nam Đồng",
            "district_code": "0004"
        },
        {
            "code": "0043",
            "name": "Ngã Tư Sở",
            "district_code": "0004"
        },
        {
            "code": "0044",
            "name": "Ô Chợ Dừa",
            "district_code": "0004"
        },
        {
            "code": "0045",
            "name": "Phương Liên",
            "district_code": "0004"
        },
        {
            "code": "0046",
            "name": "Phương Mai",
            "district_code": "0004"
        },
        {
            "code": "0047",
            "name": "Quan Trung",
            "district_code": "0004"
        },
        {
            "code": "0048",
            "name": "Quốc Tử Giám",
            "district_code": "0004"
        },
        {
            "code": "0049",
            "name": "Thịnh Quang",
            "district_code": "0004"
        },
        {
            "code": "0050",
            "name": "Thổ Quan",
            "district_code": "0004"
        },
        {
            "code": "0051",
            "name": "Trung Liệt",
            "district_code": "0004"
        },
        {
            "code": "0052",
            "name": "Trung Phụng",
            "district_code": "0004"
        },
        {
            "code": "0053",
            "name": "Trung Tự",
            "district_code": "0004"
        },
        {
            "code": "0054",
            "name": "Văn Chương",
            "district_code": "0004"
        },
        {
            "code": "0055",
            "name": "Văn Miếu",
            "district_code": "0004"
        },
        {
            "code": "0056",
            "name": "Biên Giang",
            "district_code": "0005"
        },
        {
            "code": "0057",
            "name": "Đồng Mai",
            "district_code": "0005"
        },
        {
            "code": "0058",
            "name": "Yên Nghĩa",
            "district_code": "0005"
        },
        {
            "code": "0059",
            "name": "Dương Nội",
            "district_code": "0005"
        },
        {
            "code": "0060",
            "name": "Hà Cầu",
            "district_code": "0005"
        },
        {
            "code": "0061",
            "name": "La Khê",
            "district_code": "0005"
        },
        {
            "code": "0062",
            "name": "Mộ Lao",
            "district_code": "0005"
        },
        {
            "code": "0063",
            "name": "Nguyễn Trãi",
            "district_code": "0005"
        },
        {
            "code": "0064",
            "name": "Phú La",
            "district_code": "0005"
        },
        {
            "code": "0065",
            "name": "Phúc Lãm",
            "district_code": "0005"
        },
        {
            "code": "0066",
            "name": "Phú Lương",
            "district_code": "0005"
        },
        {
            "code": "0067",
            "name": "Kiến Hưng",
            "district_code": "0005"
        },
        {
            "code": "0068",
            "name": "Phúc La",
            "district_code": "0005"
        },
        {
            "code": "0069",
            "name": "Quang Trung",
            "district_code": "0005"
        },
        {
            "code": "0070",
            "name": "Vạn Phúc",
            "district_code": "0005"
        },
        {
            "code": "0071",
            "name": "Văn Quán",
            "district_code": "0005"
        },
        {
            "code": "0072",
            "name": "Yết Kiêu",
            "district_code": "0005"
        },
        {
            "code": "0073",
            "name": "Bách Khoa",
            "district_code": "0006"
        },
        {
            "code": "0074",
            "name": "Bạch Đằng",
            "district_code": "0006"
        },
        {
            "code": "0075",
            "name": "Bạch Mai",
            "district_code": "0006"
        },
        {
            "code": "0076",
            "name": "Cầu Dền",
            "district_code": "0006"
        },
        {
            "code": "0077",
            "name": "Đồng Mác",
            "district_code": "0006"
        },
        {
            "code": "0078",
            "name": "Đồng Nhân",
            "district_code": "0006"
        },
        {
            "code": "0079",
            "name": "Đồng Tâm",
            "district_code": "0006"
        },
        {
            "code": "0080",
            "name": "Lê Đại Hành",
            "district_code": "0006"
        },
        {
            "code": "0081",
            "name": "Minh Khai",
            "district_code": "0006"
        },
        {
            "code": "0082",
            "name": "Nguyễn Du",
            "district_code": "0006"
        },
        {
            "code": "0083",
            "name": "Phạm Đình Hổ",
            "district_code": "0006"
        },
        {
            "code": "0084",
            "name": "Phố Huế",
            "district_code": "0006"
        },
        {
            "code": "0085",
            "name": "Quỳnh Mai",
            "district_code": "0006"
        },
        {
            "code": "0086",
            "name": "Thanh Lương",
            "district_code": "0006"
        },
        {
            "code": "0087",
            "name": "Thanh Nhàn",
            "district_code": "0006"
        },
        {
            "code": "0088",
            "name": "Trương Định",
            "district_code": "0006"
        },
        {
            "code": "0089",
            "name": "Vĩnh Tuy",
            "district_code": "0006"
        },
        {
            "code": "0090",
            "name": "Chương Dương",
            "district_code": "0007"
        },
        {
            "code": "0091",
            "name": "Cửa Đông",
            "district_code": "0007"
        },
        {
            "code": "0092",
            "name": "Cửa Nam",
            "district_code": "0007"
        },
        {
            "code": "0093",
            "name": "Đồng Xuân",
            "district_code": "0007"
        },
        {
            "code": "0094",
            "name": "Hàng Bạc",
            "district_code": "0007"
        },
        {
            "code": "0095",
            "name": "Hàng Bài",
            "district_code": "0007"
        },
        {
            "code": "0096",
            "name": "Hàng Bồ",
            "district_code": "0007"
        },
        {
            "code": "0097",
            "name": "Hàng Bông",
            "district_code": "0007"
        },
        {
            "code": "0098",
            "name": "Hàng Buồm",
            "district_code": "0007"
        },
        {
            "code": "0099",
            "name": "Hàng Đào",
            "district_code": "0007"
        },
        {
            "code": "0100",
            "name": "Hàng Gai",
            "district_code": "0007"
        },
        {
            "code": "0101",
            "name": "Hàng Mã",
            "district_code": "0007"
        },
        {
            "code": "0102",
            "name": "Hàng Trống",
            "district_code": "0007"
        },
        {
            "code": "0103",
            "name": "Lý Thái Tổ",
            "district_code": "0007"
        },
        {
            "code": "0104",
            "name": "Phan Chu Trinh",
            "district_code": "0007"
        },
        {
            "code": "0105",
            "name": "Phúc Tân",
            "district_code": "0007"
        },
        {
            "code": "0106",
            "name": "Trần Hưng Đạo",
            "district_code": "0007"
        },
        {
            "code": "0107",
            "name": "Tràng Tiền",
            "district_code": "0007"
        },
        {
            "code": "0108",
            "name": "Đại Kim",
            "district_code": "0008"
        },
        {
            "code": "0109",
            "name": "Định Công",
            "district_code": "0008"
        },
        {
            "code": "0110",
            "name": "Giáp Bát",
            "district_code": "0008"
        },
        {
            "code": "0111",
            "name": "Hoàng Liệt",
            "district_code": "0008"
        },
        {
            "code": "0112",
            "name": "Hoàng Văn Thụ",
            "district_code": "0008"
        },
        {
            "code": "0113",
            "name": "Lĩnh Nam",
            "district_code": "0008"
        },
        {
            "code": "0114",
            "name": "Mai Động",
            "district_code": "0008"
        },
        {
            "code": "0115",
            "name": "Tân Mai",
            "district_code": "0008"
        },
        {
            "code": "0116",
            "name": "Tây Tựu",
            "district_code": "0008"
        },
        {
            "code": "0117",
            "name": "Thanh Trì",
            "district_code": "0008"
        },
        {
            "code": "0118",
            "name": "Thanh Liệt",
            "district_code": "0008"
        },
        {
            "code": "0119",
            "name": "Trần Phú",
            "district_code": "0008"
        },
        {
            "code": "0120",
            "name": "Tương Mai",
            "district_code": "0008"
        },
        {
            "code": "0121",
            "name": "Vĩnh Hưng",
            "district_code": "0008"
        },
        {
            "code": "0122",
            "name": "Yên Sở",
            "district_code": "0008"
        },
        {
            "code": "0123",
            "name": "Bồ Đề",
            "district_code": "0009"
        },
        {
            "code": "0124",
            "name": "Cự Khối",
            "district_code": "0009"
        },
        {
            "code": "0125",
            "name": "Đức Giang",
            "district_code": "0009"
        },
        {
            "code": "0126",
            "name": "Gia Thụy",
            "district_code": "0009"
        },
        {
            "code": "0127",
            "name": "Giang Biên",
            "district_code": "0009"
        },
        {
            "code": "0128",
            "name": "Long Biên",
            "district_code": "0009"
        },
        {
            "code": "0129",
            "name": "Ngọc Lâm",
            "district_code": "0009"
        },
        {
            "code": "0130",
            "name": "Ngọc Thụy",
            "district_code": "0009"
        },
        {
            "code": "0131",
            "name": "Phúc Đồng",
            "district_code": "0009"
        },
        {
            "code": "0132",
            "name": "Phúc Lợi",
            "district_code": "0009"
        },
        {
            "code": "0133",
            "name": "Sài Đồng",
            "district_code": "0009"
        },
        {
            "code": "0134",
            "name": "Thạch Bàn",
            "district_code": "0009"
        },
        {
            "code": "0135",
            "name": "Thượng Thanh",
            "district_code": "0009"
        },
        {
            "code": "0136",
            "name": "VIệt Hưng",
            "district_code": "0009"
        },
        {
            "code": "0137",
            "name": "Cầu Diễn",
            "district_code": "0010"
        },
        {
            "code": "0138",
            "name": "Mỹ Đình 1",
            "district_code": "0010"
        },
        {
            "code": "0139",
            "name": "Mỹ Đình 2",
            "district_code": "0010"
        },
        {
            "code": "0140",
            "name": "Phú Đô",
            "district_code": "0010"
        },
        {
            "code": "0141",
            "name": "Mễ Trì",
            "district_code": "0010"
        },
        {
            "code": "0142",
            "name": "Trung văn",
            "district_code": "0010"
        },
        {
            "code": "0143",
            "name": "Đại Mỗ",
            "district_code": "0010"
        },
        {
            "code": "0144",
            "name": "Tây Mỗ",
            "district_code": "0010"
        },
        {
            "code": "0145",
            "name": "Phương Canh",
            "district_code": "0010"
        },
        {
            "code": "0146",
            "name": "Xuân Phương",
            "district_code": "0010"
        },
        {
            "code": "0147",
            "name": "Bưởi",
            "district_code": "0011"
        },
        {
            "code": "0148",
            "name": "Nhật Tân",
            "district_code": "0011"
        },
        {
            "code": "0149",
            "name": "Phú Thượng",
            "district_code": "0011"
        },
        {
            "code": "0150",
            "name": "Quảng An",
            "district_code": "0011"
        },
        {
            "code": "0151",
            "name": "Thụy Khê",
            "district_code": "0011"
        },
        {
            "code": "0152",
            "name": "Tứ Liên",
            "district_code": "0011"
        },
        {
            "code": "0153",
            "name": "Xuân La",
            "district_code": "0011"
        },
        {
            "code": "0154",
            "name": "Yên Phụ",
            "district_code": "0011"
        },
        {
            "code": "0155",
            "name": "Hạ Đình",
            "district_code": "0012"
        },
        {
            "code": "0156",
            "name": "Khương Đình",
            "district_code": "0012"
        },
        {
            "code": "0157",
            "name": "Khương Mai",
            "district_code": "0012"
        },
        {
            "code": "0158",
            "name": "Khương Trung",
            "district_code": "0012"
        },
        {
            "code": "0159",
            "name": "Kim Giang",
            "district_code": "0012"
        },
        {
            "code": "0160",
            "name": "Nhân Chính",
            "district_code": "0012"
        },
        {
            "code": "0161",
            "name": "Phường Liệt",
            "district_code": "0012"
        },
        {
            "code": "0162",
            "name": "Thanh Xuân Bắc",
            "district_code": "0012"
        },
        {
            "code": "0163",
            "name": "Thanh Xuân Nam",
            "district_code": "0012"
        },
        {
            "code": "0164",
            "name": "Thanh Xuân Trung",
            "district_code": "0012"
        },
        {
            "code": "0165",
            "name": "Thượng Đình",
            "district_code": "0012"
        }
    ]

    try:
        op.bulk_insert(table, data)
    except:
        op.drop_table('ward')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('ward')
    # ### end Alembic commands ###
