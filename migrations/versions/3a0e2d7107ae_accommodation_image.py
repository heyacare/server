"""accommodation image

Revision ID: 3a0e2d7107ae
Revises: 31867bb9d841
Create Date: 2020-12-19 17:10:19.149188

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3a0e2d7107ae'
down_revision = '31867bb9d841'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('accommodation_image',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('accommodation_id', sa.String(length=255), nullable=False),
    sa.Column('image_url', sa.String(length=255), nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('updated_at', sa.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.ForeignKeyConstraint(['accommodation_id'], ['accommodation.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('accommodation_image')
    # ### end Alembic commands ###
